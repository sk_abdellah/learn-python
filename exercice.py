# RÉPÉTITIONS
# Adapter le code précédent pour afficher tous les nombres de 1 à 100.

""" for i in range(1, 101):
    print(i) """


# Afficher un rectangle de taille 4x5 composé de "x".

""" for rectangle in range(4):
    print("XXXXX") """


# Afficher tous les nombres de à 100 à 1.

""" for j in range(100, 0, -1):
    print(j) """


# Afficher un triangle composé de 10 "o" au total.

""" for k in range(1, 5, 1):
    print(k*"o") """



# TABLEAUX
# Écrire un programme qui demande 5 prénoms, les stocke dans un tableau, puis les affiche.

""" prenoms = []
print(prenoms)

for l in range(1, 6):
    nouveau = input('Entrez un prénom : ')
    prenoms.append(nouveau)

print(prenoms) """


# DICTIONNAIRES
# Compléter ce code pour afficher les films diffusés après 12h :

horaires = [
    {'film': 'Seul sur Mars', 'heure': 9},
    {'film': 'Astérix et Obélix Mission Cléopâtre', 'heure': 10},
    {'film': 'Star Wars VII', 'heure': 15},
    {'film': 'Time Lapse', 'heure': 18},
    {'film': 'Fatal', 'heure': 20},
    {'film': 'Limitless', 'heure': 20},
]

for seance in horaires:
    if seance['heure']>12:
        print(seance['film'], 'est diffusé a', seance['heure'], 'h')